<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>JPNET</title>
        <link rel="stylesheet" href="{{ URL::asset('css/x.css') }}">   
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Biryani' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Archivo Black' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Cutive Mono' rel='stylesheet'>

    </head>
    <body>
    
        <div class="flex-center position-ref full-height" class="static">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                     </div>
                     @endif
            
           
                     
                        <div class="content">
                        <div>
                                <ul>
                                <li><a href="home">Home</a></li>
                               <!-- <li><a href="#news">News</a></li> -->
                                <li><a href="gallery">Gallery</a></li>
                                <li><a href="about">About</a></li>
                                </ul>
                            </div>
                     <br>
                            <div class="container">   
                                                                <br>
                                    <div class="title m-b-md">
                                    
                                       <img src="gfx/x.jpg" style="width:600px;height:300px;"/> 
                                       
                                    <div class="centered">LITRATO</div>

                         </div> 
                         <div class="jp">  
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.<br> At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>    
                        </div>
                         

                                    <div class="footer"><p>curating modern visual arts</p></div>
                        
                         
                     </div>

                </div>
        </div>
    </body>
</html>
